#include <iostream>
#include "library.h"
#include "gtest/gtest.h"

TEST(addition, additionFirst)
{
    int a { 1 }, b { 2 }, result { a + b };
    EXPECT_EQ(result, addition(a, b));
}

int main(int argc, char ** argv)
{
    std::cout << addition(1, 2) << std::endl;
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}